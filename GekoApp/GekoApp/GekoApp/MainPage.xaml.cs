﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GekoApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly string url = "https://www.geko.hn/index.php?";
        public MainPage()
        {
            InitializeComponent();
            this.webView.Source = this.url;
            this.webView.Navigating += WebView_Navigating;
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
            this.Appearing += ItemsPage_Appearing;
        }

        private void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            if (e.Url.Contains("mailto:"))
            {
                Device.OpenUri(new Uri(e.Url));
                webView.GoBack();
                e.Cancel = true;
            }
            if (e.Url.Count() > 23 && e.Url.Substring(0, 23) == $"whatsapp://send/?phone=")
            {
                Device.OpenUri(new Uri(e.Url));
                webView.GoBack();
                e.Cancel = true;
            }
            else if (e.Url.Contains("scheme=fb-messenger;package=com.facebook.orca;end"))
            {
                Device.OpenUri(new Uri($"fb-messenger://user-thread/{e.Url.Substring(14, 15)}"));
                webView.GoBack();
                e.Cancel = true;
            }
            return;
        }
        private void ItemsPage_Appearing(object sender, EventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                DisplayAlert("Sin conexión a internet", "En este momento no dispone de conexión a internet", "OK");
                webView.IsVisible = false;
            }
        }

        async void Reload(object sender, EventArgs e)
        {
            this.webView.Reload();
        }

        async void Home(object sender, EventArgs e)
        {
            this.webView.Source = this.url;
        }

        async void Shop(object sender, EventArgs e)
        {
            this.webView.Source = string.Concat(this.url, "controller=order-opc");
        }

        async void BackClicked(object sender, EventArgs e)
        {
            if (webView.CanGoBack)
            {
                webView.GoBack();
            }
            else
            {
                await Navigation.PopAsync();
            }
        }

        async void ForwardClicked(object sender, EventArgs e)
        {
            if (webView.CanGoForward)
            {
                webView.GoForward();
            }
        }

        protected override bool OnBackButtonPressed()
        {
            if (webView.CanGoBack)
            {
                webView.GoBack();
                return true;
            }
            else
            {
                Navigation.PopAsync();
                base.OnBackButtonPressed();
                return false;
            }
        }

        private async void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            if (!e.IsConnected)
            {
                await DisplayAlert("Sin conexión a internet", "En este momento no dispone de conexión a internet", "OK"); webView.IsVisible = false;
            }
            else
            {
                webView.Reload();
                webView.IsVisible = true;
            }
        }
    }
}